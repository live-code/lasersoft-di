import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  constructor() { }

  log(value: string) {
    console.log('LOG:', value)
  }
}
