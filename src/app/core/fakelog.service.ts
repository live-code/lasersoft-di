import { Injectable } from '@angular/core';

@Injectable()
export class FakelogService {
  constructor() { }

  log(value: string) {
    console.log('FAKE LOG:', value)
  }
}
