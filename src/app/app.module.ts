import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogService } from './core/log.service';
import { FakelogService } from './core/fakelog.service';
import { environment } from '../environments/environment';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule.forRoot()
  ],
  providers: [
    {
      provide: 'API',
      useValue: { url: 'localhost', value: 123 }
    },

    FakelogService,
    // { provide: LanguageService, useClass: LanguageService},
    {
      provide: LogService,
      useFactory: () => {
        return environment.production ? new LogService() : new FakelogService()
      }
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
