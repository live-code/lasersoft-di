import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
  value = 0;

  inc() {
    this.value++;
  }

  dec() {
    this.value--;
  }
  constructor() {
    console.log('CONSTRUCTOR COUNTER')
  }
}
