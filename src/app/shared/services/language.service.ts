import { Injectable } from '@angular/core';

type Language = 'it' | 'en';

@Injectable()
export class LanguageService {
  private value: Language = 'en';

  constructor() {
    console.log('CONSTRUCTOR LANGUAGE SERVICE')
  }

  set theme(val: Language) {
    this.value = val;
  }
  get theme(): Language {
    return this.value;
  }
}
