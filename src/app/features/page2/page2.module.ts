import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page2RoutingModule } from './page2-routing.module';
import { Page2Component } from './page2.component';
import { GoogleanalyticsService } from '../../core/googleanalytics.service';
import { LogService } from '../../core/log.service';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    Page2Component
  ],
  imports: [
    CommonModule,
    Page2RoutingModule,
    SharedModule
  ],
  providers: [
    { provide: LogService, useClass: GoogleanalyticsService }
  ]
})
export class Page2Module { }
