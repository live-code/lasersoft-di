import { Component, OnInit } from '@angular/core';
import { LogService } from '../../core/log.service';
import { FakelogService } from '../../core/fakelog.service';
import { CounterService } from '../../shared/services/counter.service';
import { LanguageService } from '../../shared/services/language.service';
import { ConfigService } from '../page1/services/config.service';

@Component({
  selector: 'app-page2',
  template: `
    <p>
      page2 works!
    </p>
    <h1>{{languageService.theme}}</h1>
    <button (click)="languageService.theme = 'it'">it</button>
    <button (click)="languageService.theme = 'en'">en</button>
    
    <hr>
    <h1>Counter: {{counterService.value}}</h1>
    <button (click)="counterService.inc()">+</button>
  `,
  styles: [
  ]
})
export class Page2Component implements OnInit {

  constructor(
    public languageService: LanguageService, private logService: LogService, public counterService: CounterService) {
    logService.log('page 2')
  }

  ngOnInit(): void {
  }

}
