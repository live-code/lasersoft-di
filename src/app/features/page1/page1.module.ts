import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page1RoutingModule } from './page1-routing.module';
import { Page1Component } from './page1.component';
import { SharedModule } from '../../shared/shared.module';
import { ConfigService } from './services/config.service';


@NgModule({
  declarations: [
    Page1Component
  ],
  imports: [
    CommonModule,
    Page1RoutingModule,
    SharedModule
  ],
  providers: [
    ConfigService
  ]
})
export class Page1Module { }
