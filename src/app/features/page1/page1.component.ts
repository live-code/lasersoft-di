import { Component, OnInit } from '@angular/core';
import { LogService } from '../../core/log.service';
import { environment } from '../../../environments/environment';
import { CounterService } from '../../shared/services/counter.service';
import { LanguageService } from '../../shared/services/language.service';
import { ConfigService } from './services/config.service';

@Component({
  selector: 'app-page1',
  template: `
   
    <app-card></app-card>
    <h1>{{languageService.theme}}</h1>
    <button (click)="languageService.theme = 'it'">it</button>
    <button (click)="languageService.theme = 'en'">en</button>
    <hr>
  `,
  styles: [
  ]
})
export class Page1Component implements OnInit {
  constructor(
    public languageService: LanguageService,
    public logService: LogService,
    private configService: ConfigService
  ) {
    logService.log('page 1')
    console.log(languageService.theme, environment.production, environment.API)
  }

  ngOnInit(): void {
  }

}
