import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="page1">page 1</button>
    <button routerLink="page2">page 2</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

  constructor(@Inject('API') api: any) {
    console.log(api)
  }
}
